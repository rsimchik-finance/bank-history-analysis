import bank_account_reader as bank;
import lib;
import venmo_reader as venmo;

from wsgiref.simple_server import make_server;
import falcon;
import json;

PORT = 6069;
DATA_DIR = 'D:/Documents/Personal Finance/Transactions/';
KEYWORDS_FILE_NAME = 'category_keywords.json';

def get_all_transactions(should_filter_venmo, should_filter_transfers):
	# TODO scan dir and get all csv files
	venmo_filenames = venmo.get_filenames(DATA_DIR, 1, 10);
	venmo_transactions = venmo.read_transactions(venmo_filenames);

	checking_path = DATA_DIR + '23 bank checking.csv';
	checking_transactions = bank.read_transactions(checking_path);
	if (should_filter_venmo):
		checking_transactions = [t for t in checking_transactions if not bank.is_venmo(t)];
	if (should_filter_transfers):
		checking_transactions = [t for t in checking_transactions if not bank.is_transfer(t)];
		venmo_transactions = [t for t in venmo_transactions if not venmo.is_transfer(t)];

	transactions = venmo_transactions + checking_transactions;
	transactions.sort(key=lambda t: t['date']);
	return transactions;

def categorize_transactions(transactions):
	category_keywords = lib.load_json(DATA_DIR + KEYWORDS_FILE_NAME);
	return lib.categorize_all_transactions(transactions, category_keywords);

class TransactionsResource:
	def on_get(self, req, resp):
		should_categorize = req.get_param_as_bool('categorize');
		should_filter_venmo = req.get_param_as_bool('remove_venmo_duplicates');
		should_filter_transfers = req.get_param_as_bool('remove_transfers');

		transactions = get_all_transactions(should_filter_venmo, should_filter_transfers);
		if (should_categorize):
			transactions = categorize_transactions(transactions);

		resp.status = falcon.HTTP_200;
		resp.content_type = falcon.MEDIA_JSON;
		resp.body = json.dumps(transactions, indent=4, sort_keys=True, default=str);
	
app = falcon.App();
app.add_route('/transactions', TransactionsResource());

if __name__ == '__main__':
	with make_server('', PORT, app) as server:
		print(f'Serving on port {PORT}...');
		server.serve_forever();
