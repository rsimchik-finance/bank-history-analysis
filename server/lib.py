import json;
import numpy;

def load_json(full_path):
	category_keywords = {};
	with open(full_path, 'r', encoding='utf-8') as f:
		category_keywords = json.load(f);

	return category_keywords;

def remove_all(chars, from_string):
	for char in chars:
		from_string = from_string.replace(char, '');
	return from_string;

def parse_money(money):
	return float(remove_all(['$', ',', ' '], money));

def char_to_int(char):
	return ord(char) - ord('A');

def str_to_ints(str):
	return [char_to_int(char) for char in str];

def similarity(str1, str2):
	if (str1 in [None, ''] or str2 in [None, '']):
		return 0;

	ints1 = str_to_ints(str1);
	ints2 = str_to_ints(str2);
	similarity = numpy.correlate(ints1, ints2, 'full').max();
	max_similarity = max([
		numpy.sum([i**2 for i in ints1]),
		numpy.sum([i**2 for i in ints2]),
	]);
	return similarity / max_similarity;

def fuzzy_find(needle, haystack, threshold):
	if (needle in [None, ''] or len(haystack) == 0):
		return None;

	if (needle in haystack):
		return needle;

	best_match = None;
	best_similarity = 0;
	for straw in haystack:
		if (similarity(needle, straw) > best_similarity):
			best_match = straw;
			best_similarity = similarity(needle, straw);
	
	if (best_similarity < threshold):
		return None;
	return best_match;

def filter(name, filter_type, rule):
	if (name in [None, ''] or rule in [None, ''] or len(rule) == 0):
		return False;

	match filter_type:
		case 'contains': return any([
			keyword.lower() in name.lower()
			for keyword in rule
		]);
		case 'equals': return any([
			keyword.lower() == name.lower()
   		for keyword in rule
		]);
		case 'similar to': 
			return fuzzy_find(name, rule, 0.9) != None;
	
	return False;

def matches_any_filter(name, filters):
	return any([
		filter(name, filter_type, filters[filter_type])
		for filter_type in filters
	]);

def first_matching_group(name, filter_groups):
	for group_name in filter_groups:
		filters = filter_groups[group_name];
		if matches_any_filter(name, filters):
			return group_name;
		
	return None;

def init_category():
	return {
		'count': 0,
		'amounts': [],
		'variants': [],
	};

def init_transaction(date, amount, type, description):
  return {
		'date': date,
		'amount': amount,
		'type': type,
		'description': description,
	};

# Expects 'Date', 'Type', 'Description', 'Amount'.
# Don't be afraid to dissolve this abstraction if you have to
# fight it to add new cases.
def transaction_from_csv(row, columns, parse_date):
	type = row[columns['Type']];
	if (type in ['Type', 'Description', '']):
		return None;
	
	return init_transaction(
		parse_date(row[columns['Date']]),
		parse_money(row[columns['Amount']]),
		type,
		row[columns['Description']]
	);

def categorize_transaction(transaction, categories, category_filters):
	name = transaction['description'];
	if (name in [None, '']):
		return None;

	# user-defined filters get first priority...
	category = first_matching_group(name, category_filters);
	if category is not None:
		return category;

	# then exact matches...
	if (name in categories):
		return name;
	
	# then fuzzy matches...
	category = fuzzy_find(name, categories, 0.9);
	if (category is not None):
		return category;
	
	# sad :(
	return None;

def categorize_all_transactions(transactions, filter_groups):
	categories = {};

	for transaction in transactions:
		category = categorize_transaction(transaction, categories, filter_groups);
		if category is None:
			category = transaction['description'];

		if category not in categories:
			categories[category] = [];
		categories[category].append(transaction);
	
	return categories;
