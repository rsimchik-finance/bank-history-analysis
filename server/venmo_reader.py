import lib;

import csv;
from datetime import datetime;

# months are 1-indexed
def get_filenames(root_dir, start_month, end_month):
	venmo_format = '{year:02d} {month:02d} venmo.csv';
	year = 23;
	return [
		root_dir + venmo_format.format(year=year, month=month)
		for month in range(start_month, end_month)
	];

def parse_venmo_date(date):
  return datetime.strptime(date, '%Y-%m-%dT%H:%M:%S');

def venmo_columns():
	return {
   'Date': 2,
	 'Type': 3,
	 'Description': 5,
	 'Amount': 8,
	};

def transaction_from_venmo(row):
	return lib.transaction_from_csv(row, venmo_columns(), parse_venmo_date);

def is_transfer(transaction):
	return 'Transfer' in transaction['type'];

def combine_csv(full_paths, encoding='utf-8'):
	all_rows = [];
	for path in full_paths:
		with open(path, 'r', encoding=encoding) as f:
			reader = csv.reader(f);
			for row in reader:
				all_rows.append(row);
	
	return all_rows;

def summarize_transactions(categorized_transactions):
	summary = {};
	for category in categorized_transactions:
		transactions = categorized_transactions[category];
		transactions.sort(key=lambda t: t['date']);
		min_month = transactions[0]['date'].month;
		max_month = transactions[-1]['date'].month;

		monthly_balances = [0 for i in range(min_month, max_month + 1)];
		for transaction in transactions:
			monthly_balances[transaction['date'].month - min_month] += transaction['amount'];
		
		average = sum(monthly_balances) / len(monthly_balances);
		deviation = sum([(balance - average)**2 for balance in monthly_balances]) / len(monthly_balances);
		standard_deviation = deviation ** 0.5;
		summary[category] = {
			'num_transactions': len(transactions),
			'monthly_balances': monthly_balances,
			'average_monthly_balance': average,
			'deviation': standard_deviation,
		};

	return summary;

def read_transactions(full_paths):
	venmo_rows = combine_csv(full_paths);
	venmo_transactions = [t for row in venmo_rows if (t := transaction_from_venmo(row)) is not None];
	return venmo_transactions;

def process_venmo_files(full_paths):
	venmo_transactions = read_transactions(full_paths);
	category_keywords_path = './category_keywords.json';
	category_keywords = load_json(category_keywords_path);
	categorized_transactions = categorize_all_transactions(venmo_transactions, category_keywords);
	summary = summarize_transactions(categorized_transactions);

	return summary;

def print_venmo_summary(summary):
	for category in summary:
		print(category);
		print('Transactions: ' + str(summary[category]['num_transactions']));
		print('Monthly Cost: ' + str(summary[category]['monthly_balances']));
		print('Average Monthly Cost: ' + str(summary[category]['average_monthly_balance']));
		print('Deviation: ' + str(summary[category]['deviation']));
		print();
