import lib;

import csv;
from datetime import datetime;

def parse_bank_date(date_str):
	return datetime.strptime(date_str, '%Y/%m/%d');

def bank_columns():
	return {
		'Date': 3,
		'Amount': 4,
		'Type': 5,
		'Description': 6,
	};

def transaction_from_bank(row):
	return lib.transaction_from_csv(row, bank_columns(), parse_bank_date);

def read_transactions(full_path):
	transactions = [];
	with open(full_path, 'r') as f:
		reader = csv.reader(f);
		for row in reader:
			t = transaction_from_bank(row);
			if t is not None:
				transactions.append(t);
	
	return transactions;

def is_venmo(transaction):
  return 'VENMO' in transaction['description'];

def is_transfer(transaction):
	return 'Transfer' in transaction['type'];

def init_checking_state():
	return {
		'total_expenses': 0,
		'total_income': 0,
		'cash_widthdrawn': 0,
		'categories': {},
	}

def is_category_bill(category):
	count = category[1]['count'];
	amounts = category[1]['amounts'];
	return count > 2 and len(amounts) < 3;

def print_category(category):
	name = category[0];
	count = category[1]['count'];
	amounts = category[1]['amounts'];
	variants = category[1]['variants'];
	print(name);
	print('\tCount: ' + str(count));
	print('\tAmounts: ' + str(amounts));
	if (len(variants) > 0):
		for variant in variants:
			print('\tVariant: ' + variant);

# assumes transactions are read newest to oldest
def process_checking(row, state):
	transaction = transaction_from_bank(row);
	if (transaction == None):
		return;

	amount = transaction['amount'];
	type = transaction['type'];
	description = transaction['description'];

	if (not 'Transfer' in type):
		if (amount > 0):
			state['total_income'] += amount;
		elif (amount < 0):
			state['total_expenses'] += amount;
			
			if (description != None and description != ''):
				category = lib.categorize_transaction(description, state['categories']);
				category['count'] += 1;
				if (not amount in category['amounts']):
					category['amounts'].append(amount);

	if (type == 'ATM Withdrawal'):
		state['cash_widthdrawn'] += amount;

def process_checking_file(full_path):
	checking_state = init_checking_state();
	with open(full_path, 'r') as f:
		reader = csv.reader(f);
		for row in reader:
			process_checking(row, checking_state);

	return checking_state;

def stringify_checking(state):
    for key in ['total_expenses', 'total_income', 'cash_widthdrawn']:
        state[key] = format(state[key], '.2f');