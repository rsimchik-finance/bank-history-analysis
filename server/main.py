import bank_account_reader as bank;
import lib;
import venmo_reader as venmo;

root_dir = 'D:/Documents/Personal Finance/Transactions/';

venmo_filenames = venmo.get_filenames(root_dir, 1,9);
venmo_transactions = venmo.read_transactions(venmo_filenames);

savings_path = root_dir + '23 bank savings.csv';
savings_transactions = bank.read_transactions(savings_path);

transactions = venmo_transactions + savings_transactions;
transactions.sort(key=lambda t: t['date']);
	
savings_config_path = root_dir + 'savings_config.json';
savings_config = lib.load_json(savings_config_path);

net_salary = savings_config['salary']['net_annual'];
monthly_pay = net_salary / 12;
pay_contribution = savings_config['salary']['contribution'];
savings_balance = 2000;
for account in savings_config['accounts']:
	balance = savings_balance * account['contribution'];
	print('{}: ${:.2f}'.format(account['name'], balance / 100));
