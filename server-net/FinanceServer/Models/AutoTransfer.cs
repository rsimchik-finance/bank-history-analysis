﻿namespace FinanceServer.Models
{
    public class AutoTransfer : IModel
    {
        public int Id { get; set; }

        public MoneyAccount Source { get; set; } 
        public MoneyAccount Destination { get; set; }
        public decimal Contribution { get; set; }
        public bool ShouldSubtractFromSource { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
