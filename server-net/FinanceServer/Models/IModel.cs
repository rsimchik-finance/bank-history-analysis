﻿namespace FinanceServer.Models
{
    public interface IModel
    {
        public int Id { get; }
    }
}
