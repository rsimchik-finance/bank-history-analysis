﻿namespace FinanceServer.Models
{
    public class Transaction : IModel
    {
        public int Id { get; set; }
        public MoneyAccount? Account { get; set; }

        public int Amount { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Timestamp { get; set; }

        public string? SourceId { get; set; }
    }
}
