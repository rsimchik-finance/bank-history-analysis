﻿namespace FinanceServer.Models.Dtos
{
    public class TransactionDto
    {
        public class Create
        {
            public int Amount { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public DateTime Timestamp { get; set; }
            public string? SourceId { get; set; }

            public Transaction ToModel()
            {
                return new Transaction
                {
                    Id = 0,
                    Account = null,

                    Amount = Amount,
                    Title = Title,
                    Description = Description,
                    Timestamp = Timestamp,
                    SourceId = SourceId,
                };
            }
        }
    }
}
