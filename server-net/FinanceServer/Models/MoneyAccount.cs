﻿namespace FinanceServer.Models
{
    public class MoneyAccount : IModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Balance { get; set; }

        public int? TargetBalance { get; set; }
        public DateTime? TargetDate { get; set; }

        public ICollection<Transaction> Transactions { get; set; }
        public ICollection<AutoTransfer> OutgoingAutoTransfers { get; set; }
        public ICollection<AutoTransfer> IncomingAutoTransfers { get; set; }
    }
}
