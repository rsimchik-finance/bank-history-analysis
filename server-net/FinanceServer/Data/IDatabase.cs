﻿namespace FinanceServer.Data
{
    public interface IDatabase
    {
        public string GetConnectionString();
        public void SetModified<T>(T entity);
    }
}
