﻿using FinanceServer.Models;

using Microsoft.EntityFrameworkCore;
using MySql.EntityFrameworkCore.Extensions;

namespace FinanceServer.Data
{
    public class FinanceDatabase : DbContext, IDatabase
    {
        public virtual DbSet<MoneyAccount> MoneyAccounts { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<AutoTransfer> AutoTransfers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL(GetConnectionString());
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<MoneyAccount>(entity =>
            {
                entity.HasKey(ma => ma.Id);
                entity.Property(ma => ma.Name).IsRequired();
            });

            modelBuilder.Entity<Transaction>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.HasOne(t => t.Account).WithMany(ma => ma.Transactions);
            });

            modelBuilder.Entity<AutoTransfer>(entity =>
            {
                entity.HasKey(at => at.Id);
                entity.HasOne(at => at.Source)
                    .WithMany(ma => ma.OutgoingAutoTransfers);
                entity.HasOne(at => at.Destination)
                    .WithMany(ma => ma.IncomingAutoTransfers);
            });
        }

        public virtual string GetConnectionString()
        {
            return "server=127.0.0.1;port=3316;database=PersonalFinance;uid=financeapp;pwd=probablyefcoreidk";
        }

        public virtual void SetModified<T>(T entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        private void EnableSQLLogging(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLoggerFactory(LoggerFactory.Create(builder =>
            {
                builder.AddFilter((category, level) =>
                        (category == DbLoggerCategory.ChangeTracking.Name |
                         category == DbLoggerCategory.Database.Transaction.Name |
                         category == DbLoggerCategory.Database.Command.Name)
                         && level == LogLevel.Debug
                     ).AddDebug();
                builder.AddConsole();
            }));
            optionsBuilder.EnableSensitiveDataLogging();
        }
    }
}
