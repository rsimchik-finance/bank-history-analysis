﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FinanceServer.Data.Migrations
{
    /// <inheritdoc />
    public partial class AddTitleDescriptionSourceIdToTransaction : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Transactions",
                type: "longtext",
                nullable: false);

            migrationBuilder.AddColumn<string>(
                name: "SourceId",
                table: "Transactions",
                type: "longtext",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Transactions",
                type: "longtext",
                nullable: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "SourceId",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Transactions");
        }
    }
}
