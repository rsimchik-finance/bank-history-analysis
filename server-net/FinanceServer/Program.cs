using FinanceServer.Data;
using FinanceServer.Repositories;
using FinanceServer.Models;
using FinanceServer.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddDbContext<FinanceDatabase>();

builder.Services.AddTransient<FinanceDatabase, FinanceDatabase>();

builder.Services.AddSingleton<IRepository<MoneyAccount>, AccountsRepository>();

builder.Services.AddSingleton<IRepository<Transaction>, TransactionsRepository>();

builder.Services.AddSingleton<IAccountsService, AccountsService>();

builder.Services.AddControllers().AddNewtonsoftJson(options =>
{
    options.SerializerSettings.ReferenceLoopHandling =
        Newtonsoft.Json.ReferenceLoopHandling.Ignore;
});

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
