﻿using FinanceServer.Models;
using FinanceServer.Models.Dtos;
using FinanceServer.Repositories;
using FinanceServer.Services;

using Microsoft.AspNetCore.Mvc;

namespace FinanceServer.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly IRepository<MoneyAccount> _accounts;
        private readonly IAccountsService _accountsService;

        public AccountsController(
            IRepository<MoneyAccount> accountsRepository,
            IAccountsService accountsService
        ) {
            _accounts = accountsRepository;
            _accountsService = accountsService;
        }

        [HttpGet("{name}")]
        public async Task<ActionResult<MoneyAccount>> GetAccount(string name)
        {
            if (string.IsNullOrEmpty(name))
                return BadRequest("Cannot get account without name.");

            var accounts = (await _accounts.GetAsync(ma => ma.Name == name));

            if (accounts.Count > 1)
                return StatusCode(500, $"Found multiple accounts named \"{name}\". This must be " +
                    "corrected by an administrator.");
            if (accounts.Count == 0)
                return NoContent();

            return Ok(accounts.First());
        }

        [HttpPost("{accountId}/CreateTransaction")]
        public async Task<ActionResult<MoneyAccount>> CreateTransaction(
            int accountId,
            TransactionDto.Create newTransaction
        ) {
            var account = await _accounts.GetSingleAsync(accountId);
            if (account == null)
                return BadRequest($"No account found with ID: {accountId}");

            var transaction = newTransaction.ToModel();
            await _accountsService.CreateAndApplyTransaction(transaction, account);

            return Ok(account);
        }
    }
}
