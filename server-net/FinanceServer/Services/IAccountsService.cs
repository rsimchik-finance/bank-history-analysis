﻿using FinanceServer.Models;

namespace FinanceServer.Services
{
    public interface IAccountsService
    {
        public Task CreateAndApplyTransaction(Transaction transaction, MoneyAccount account);
        public Task CreateAndApplyAllTransactions(
            IEnumerable<Transaction> transactions,
            MoneyAccount account
        );
    }
}
