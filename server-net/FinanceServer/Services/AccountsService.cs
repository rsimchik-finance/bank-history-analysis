﻿using FinanceServer.Models;
using FinanceServer.Repositories;

namespace FinanceServer.Services
{
    public class AccountsService : IAccountsService
    {
        private readonly IRepository<MoneyAccount> _accounts;
        private readonly IRepository<Transaction> _transactions;

        public AccountsService(
            IRepository<MoneyAccount> accounts,
            IRepository<Transaction> transactions
        ) {
            _accounts = accounts;
            _transactions = transactions;
        }

        public async Task CreateAndApplyTransaction(
            Transaction transaction, 
            MoneyAccount account
        ) {
            ApplyTransaction(transaction, account);

            await _transactions.SaveAsync();
            await _accounts.SaveAsync();
        }

        public async Task CreateAndApplyAllTransactions(
            IEnumerable<Transaction> transactions,
            MoneyAccount account
        ) {
            try
            {
                foreach (Transaction transaction in transactions)
                    ApplyTransaction(transaction, account);
            }
            catch (InvalidOperationException)
            {
                var modifiedTransactions = transactions.Where(t => _transactions.HasChanges(t));
                foreach (Transaction transaction in modifiedTransactions)
                    _transactions.UndoChanges(transaction);

                _accounts.UndoChanges(account);

                throw;
            }

            await _transactions.SaveAsync();
            await _accounts.SaveAsync();
        }

        private void ApplyTransaction(Transaction transaction, MoneyAccount account)
        {
            if (transaction.Account != null && transaction.Account != account)
            {
                throw new InvalidOperationException($"Cannot apply transaction to account \"{account.Name}" +
                    $"\" as it is already assigned to \"{transaction.Account.Name}\".");
            }

            transaction.Account = account;
            account.Transactions.Add(transaction);
            account.Balance += transaction.Amount;

            ApplyAutoTransfers(transaction, account);
        }

        private void ApplyAutoTransfers(Transaction transaction, MoneyAccount account)
        {
            var activeTransfers = account.OutgoingAutoTransfers.Where(t =>
                (t.StartDate <= transaction.Timestamp) &&
                (t.EndDate == null || t.EndDate > transaction.Timestamp)
            );
            foreach (var transfer in activeTransfers)
            {
                var transferAmount = (int)(transaction.Amount * transfer.Contribution);
                var outgoingTransaction = new Transaction()
                {
                    Account = null,
                    Amount = transferAmount,
                    Title = "Auto Transfer",
                    Description = $"Automatic transfer from {account.Name}",
                    Timestamp = transaction.Timestamp,
                };
                ApplyTransaction(outgoingTransaction, transfer.Destination);
                // TODO subtract from this account - create a transaction?
            }
        }
    }
}
