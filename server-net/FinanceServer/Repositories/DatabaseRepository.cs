﻿using FinanceServer.Data;
using FinanceServer.Models;

using Microsoft.EntityFrameworkCore;

using System.Linq.Expressions;

namespace FinanceServer.Repositories
{
    public abstract class DatabaseRepository<TModel, TDatabase> : IRepository<TModel> 
        where TModel : class, IModel
        where TDatabase : DbContext, IDatabase
    {
        private readonly TDatabase _db;

        protected DatabaseRepository(TDatabase db)
        {
            _db = db;
        }

        public async Task<ICollection<TModel>> GetAsync(Expression<Func<TModel, bool>>? filter = null)
        {
            return await QueryWithIncludes(_db)
                .Where(filter ?? (ma => true))
                .ToListAsync();
        }

        public async Task<TModel?> GetSingleAsync(int id)
        {
            try
            {
                return await QueryWithIncludes(_db).SingleAsync(obj => obj.Id == id);
            }
            catch (InvalidOperationException e)
            {
                return null;
            }
        }

        public void Create(TModel newObject)
        {
            _db.Set<TModel>().Add(newObject);
        }

        public void Update(TModel updatedObject)
        {
            _db.SetModified(updatedObject);
        }

        public void Delete(TModel toDelete)
        {
            _db.Set<TModel>().Remove(toDelete);
        }

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }

        public bool HasChanges(TModel objectToCheck)
        {
            throw new NotImplementedException();
        }

        public void UndoChanges(TModel changedObject)
        {
            throw new NotImplementedException();
        }

        public void UndoAllChanges()
        {
            throw new NotImplementedException();
        }

        protected abstract IQueryable<TModel> QueryWithIncludes(TDatabase db);
    }
}
