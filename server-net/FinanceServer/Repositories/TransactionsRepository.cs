﻿using FinanceServer.Data;
using FinanceServer.Models;

namespace FinanceServer.Repositories
{
    public class TransactionsRepository : DatabaseRepository<Transaction, FinanceDatabase>
    {
        public TransactionsRepository(FinanceDatabase db) : base(db) { }

        protected override IQueryable<Transaction> QueryWithIncludes(FinanceDatabase db)
        {
            return db.Transactions;
        }
    }
}
