﻿using FinanceServer.Models;

using System.Linq.Expressions;

namespace FinanceServer.Repositories
{
    public interface IRepository<TModel> where TModel : IModel
    {
        public Task<TModel?> GetSingleAsync(int id);
        public Task<ICollection<TModel>> GetAsync(Expression<Func<TModel, bool>>? filter = null);
        public void Create(TModel newObject);
        public void Update(TModel updatedObject);
        public void Delete(TModel toDelete);
        public Task SaveAsync();
        public bool HasChanges(TModel objectToCheck);
        public void UndoChanges(TModel changedObject);
        public void UndoAllChanges();
    }
}
