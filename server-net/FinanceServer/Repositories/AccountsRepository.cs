﻿using FinanceServer.Data;
using FinanceServer.Models;

using Microsoft.EntityFrameworkCore;

namespace FinanceServer.Repositories
{
    public class AccountsRepository : DatabaseRepository<MoneyAccount, FinanceDatabase>
    {
        public AccountsRepository(FinanceDatabase db) : base(db) { }

        protected override IQueryable<MoneyAccount> QueryWithIncludes(FinanceDatabase db)
        {
            return db.MoneyAccounts
                .Include(ma => ma.Transactions)
                .Include(ma => ma.OutgoingAutoTransfers)
                .Include(ma => ma.IncomingAutoTransfers);
        }
    }
}
