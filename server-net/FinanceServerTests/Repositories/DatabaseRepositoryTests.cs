using FinanceServer.Repositories;
using FinanceServer.Data;
using FinanceServer.Models;

using Moq.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using FinanceServerTests.TestUtilities;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace FinanceServerTests.Repositories
{
    public class DatabaseRepositoryTests : IClassFixture<TestFinanceDatabaseFixture>
    {
        private static readonly Fixture defaultFixture = Fixtures.CreateNonRecursiveFixture();
        private readonly TestFinanceDatabaseFixture dbFixture;

        public DatabaseRepositoryTests(TestFinanceDatabaseFixture dbFixture)
        {
            this.dbFixture = dbFixture;
        }

        [Fact]
        public void Constructor()
        {
            var repository = new AccountsRepository(dbFixture.InitDb());
            
            Assert.NotNull(repository);
        }

        [Fact]
        public async Task GetUnfiltered()
        {
            var repository = new AccountsRepository(dbFixture.InitDb());

            var accounts = await repository.GetAsync();

            Assert.True(accounts.Count >= TestFinanceDatabaseFixture.numAccounts); // accounts may have been created by other tests
        }

        [Fact]
        public async Task GetFiltered()
        {
            var repository = new AccountsRepository(dbFixture.InitDb());
            var validNames = new List<string> {"Test Account 1", "Test Account 4", "Test Account 8"};

            var items = await repository.GetAsync(a => validNames.Contains(a.Name));

            Assert.Equal(validNames.Count, items.Count);
            Assert.All(items, item => Assert.Contains(item.Name, validNames));
        }

        [Fact]
        public async Task GetNoResults()
        {
            var repository = new AccountsRepository(dbFixture.InitDb());

            var items = await repository.GetAsync(a => a.Name == "this name should not exist");

            Assert.Empty(items);
        }

        [Fact]
        public async Task GetSingle()
        {
            var repository = new AccountsRepository(dbFixture.InitDb());

            var expectedId = TestFinanceDatabaseFixture.numAccounts / 2;
            var actual = await repository.GetSingleAsync(expectedId);

            Assert.NotNull(actual);
            Assert.Equal(expectedId, actual.Id); 
        }

        [Fact]
        public async Task GetSingleNotFound()
        {
            var repository = new AccountsRepository(dbFixture.InitDb());

            var nonExistentId = TestFinanceDatabaseFixture.numAccounts * 2;
            var actual = await repository.GetSingleAsync(nonExistentId);

            Assert.Null(actual);
        }

        [Fact]
        public async Task Create()
        {
            const int initialId = 0;
            var repository = new AccountsRepository(dbFixture.InitDb());
            var newModel = MakeAccount();

            repository.Create(newModel);
            await repository.SaveAsync();

            var actual = (await repository.GetAsync(ma => ma.Name == newModel.Name)).FirstOrDefault();
            Assert.Equal(newModel, actual);
            Assert.NotEqual(initialId, newModel.Id);
        }

        [Fact]
        public async Task CreateNoSave()
        {
            const int initialId = 0;
            var repository = new AccountsRepository(dbFixture.InitDb());
            var newModel = MakeAccount();

            repository.Create(newModel);

            var actual = (await repository.GetAsync(ma => ma.Name == newModel.Name)).FirstOrDefault();
            Assert.Null(actual);
            Assert.Equal(initialId, newModel.Id);
        }

        [Fact]
        public async Task Delete()
        {
            var repository = new AccountsRepository(dbFixture.InitDb());
            var newModel = MakeAccount();
            repository.Create(newModel);
            await repository.SaveAsync();

            repository.Delete(newModel);
            await repository.SaveAsync();

            var actual = (await repository.GetAsync(ma => ma.Name == newModel.Name)).FirstOrDefault();
            Assert.Null(actual);
        }

        [Fact]
        public async Task DeleteNoSave()
        {
            var repository = new AccountsRepository(dbFixture.InitDb());
            var newModel = MakeAccount();
            repository.Create(newModel);
            await repository.SaveAsync();

            repository.Delete(newModel);

            var actual = (await repository.GetAsync(ma => ma.Name == newModel.Name)).FirstOrDefault();
            Assert.NotNull(actual);
        }

        [Fact]
        public async Task Update()
        {
            var repository = new AccountsRepository(dbFixture.InitDb());
            var newModel = MakeAccount();
            repository.Create(newModel);
            await repository.SaveAsync();

            var oldName = newModel.Name;
            var newName = oldName + " updated";
            newModel.Name = newName;
            repository.Update(newModel);
            await repository.SaveAsync();

            var oldNamedObj = (await repository.GetAsync(ma => ma.Name == oldName)).FirstOrDefault();
            var actual = (await repository.GetAsync(ma => ma.Name == newName)).FirstOrDefault();
            Assert.Null(oldNamedObj);
            Assert.NotNull(actual);
        }

        [Fact]
        public async Task UpdateNoSave()
        {
            var repository = new AccountsRepository(dbFixture.InitDb());
            var newModel = MakeAccount();
            repository.Create(newModel);
            await repository.SaveAsync();

            var oldName = newModel.Name;
            var newName = oldName + " updated";
            newModel.Name = newName;
            repository.Update(newModel);

            var oldNamedObj = (await repository.GetAsync(ma => ma.Name == oldName)).FirstOrDefault();
            var actual = (await repository.GetAsync(ma => ma.Name == newName)).FirstOrDefault();
            Assert.NotNull(oldNamedObj);
            Assert.Null(actual);
        }

        [Fact]
        public async Task CombinedOperations()
        {
            var repository = new AccountsRepository(dbFixture.InitDb());
            var toUpdate = MakeAccount();
            var toDelete = MakeAccount();
            repository.Create(toUpdate);
            repository.Create(toDelete);
            await repository.SaveAsync();

            var toCreate = MakeAccount();
            repository.Create(toCreate);
            var updatedName = toUpdate.Name + " updated";
            toUpdate.Name = updatedName;
            repository.Update(toUpdate);
            repository.Delete(toDelete);
            await repository.SaveAsync();

            var created = (await repository.GetAsync(ma => ma.Name == toCreate.Name)).FirstOrDefault();
            var updated = (await repository.GetAsync(ma => ma.Name == updatedName)).FirstOrDefault();
            var deleted = (await repository.GetAsync(ma => ma.Name == toDelete.Name)).FirstOrDefault();
            Assert.NotNull(created);
            Assert.NotNull(updated);
            Assert.Null(deleted);
        }

        [Fact]
        public async Task CombinedOperationsNoSave()
        {
            var repository = new AccountsRepository(dbFixture.InitDb());
            var toUpdate = MakeAccount();
            var toDelete = MakeAccount();
            repository.Create(toUpdate);
            repository.Create(toDelete);
            await repository.SaveAsync();

            var toCreate = MakeAccount();
            repository.Create(toCreate);
            var updatedName = toUpdate.Name + " updated";
            toUpdate.Name = updatedName;
            repository.Update(toUpdate);
            repository.Delete(toDelete);

            var created = (await repository.GetAsync(ma => ma.Name == toCreate.Name)).FirstOrDefault();
            var updated = (await repository.GetAsync(ma => ma.Name == updatedName)).FirstOrDefault();
            var deleted = (await repository.GetAsync(ma => ma.Name == toDelete.Name)).FirstOrDefault();
            Assert.Null(created);
            Assert.Null(updated);
            Assert.NotNull(deleted);
        }

        private MoneyAccount MakeAccount()
        {
            return defaultFixture.Build<MoneyAccount>()
                .With(ma => ma.Id, 0)
                .With(ma => ma.Transactions, new List<Transaction>())
                .With(ma => ma.IncomingAutoTransfers, new List<AutoTransfer>())
                .With(ma => ma.OutgoingAutoTransfers, new List<AutoTransfer>())
                .Create();
        }
    }

    public class TestFinanceDatabaseFixture
    {
        public const int numAccounts = 10;
        public const int numTransactionsPerAccount = 5;

        private static readonly object _lock = new();
        private static bool _databaseInitialized;

        public TestFinanceDatabaseFixture()
        {
            lock (_lock)
            {
                if (!_databaseInitialized)
                {
                    using (var db = InitDb())
                    {
                        db.Database.EnsureDeleted();
                        db.Database.EnsureCreated();

                        db.AddRange(GenerateTestAccounts());
                        db.SaveChanges();
                    }

                    _databaseInitialized = true;
                }    
            }    
        }

        public TestFinanceDatabase InitDb() => new TestFinanceDatabase();

        private static List<MoneyAccount> GenerateTestAccounts()
        {
            var accounts = new List<MoneyAccount>();
            var rand = new Random();

            for (var i = 1; i <= numAccounts; i++)
            {
                var account = new MoneyAccount()
                {
                    Id = i,
                    Name = $"Test Account {i}",
                    Balance = 0,
                    TargetBalance = rand.Next(100000) + 10000,
                    Transactions = new List<Transaction>(),
                    IncomingAutoTransfers = new List<AutoTransfer>(),
                    OutgoingAutoTransfers = new List<AutoTransfer>()
                };

                for (var j = 1; j <= numTransactionsPerAccount; j++)
                {
                    var transactionId = (i - 1) * numTransactionsPerAccount + j;
                    account.Transactions.Add(new Transaction()
                    {
                        Id = transactionId,
                        Amount = rand.Next(20000) - 10000,
                        Account = account,
                        Title = $"Transaction {transactionId}",
                        Description = $"{account.Name} - Transaction {j}"
                    });
                }

                account.Balance = account.Transactions.Sum(t => t.Amount);
                accounts.Add(account);
            }

            return accounts;
        }

    }
}