﻿using FinanceServer.Controllers;
using FinanceServer.Models;
using FinanceServer.Models.Dtos;
using FinanceServer.Repositories;
using FinanceServer.Services;

using FinanceServerTests.TestUtilities;

using Microsoft.AspNetCore.Mvc;

using System.Linq.Expressions;

namespace FinanceServerTests.Controllers
{
    using FilterFunc = Expression<Func<MoneyAccount, bool>>;

    public class AccountsControllerTest
    {
        private static Fixture defaultFixture = Fixtures.CreateNonRecursiveFixture();

        [Fact]
        public void Controller()
        {
            var accounts = new Mock<IRepository<MoneyAccount>>();
            new AccountsController(
                accounts.Object,
                new Mock<IAccountsService>().Object
            );
        }

        [Fact]
        public async Task GetAccount()
        {
            var name = "some account name";
            var accounts = new Mock<IRepository<MoneyAccount>>();
            accounts.Setup(a => a.GetAsync(It.IsAny<FilterFunc>())).ReturnsAsync(
                new List<MoneyAccount> { 
                    defaultFixture.Build<MoneyAccount>()
                        .With(a => a.Name, name).Create()
                }
            );
            var controller = new AccountsController(
                accounts.Object,
                new Mock<IAccountsService>().Object
            );

            var result = await controller.GetAccount(name);

            var actual = (result.Result as OkObjectResult).Value as MoneyAccount;
            Assert.Equal(name, actual.Name);
        }

        [Fact]
        public async Task GetAccountNoResult()
        {
            var accounts = new Mock<IRepository<MoneyAccount>>();
            accounts.Setup(a => a.GetAsync(It.IsAny<FilterFunc>())).ReturnsAsync(
                new List<MoneyAccount>()
            );
            var controller = new AccountsController(
                accounts.Object,
                new Mock<IAccountsService>().Object
            );

            var result = await controller.GetAccount("does not exist");

            Assert.IsType<NoContentResult>(result.Result);
        }

        [Fact]
        public async Task GetAccountBadInput()
        {
            var accounts = new Mock<IRepository<MoneyAccount>>();
            var controller = new AccountsController(
                accounts.Object,
                new Mock<IAccountsService>().Object
            );

            var result = await controller.GetAccount("");

            Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        [Fact]
        public async Task GetAccountMultipleResults()
        {
            var accounts = new Mock<IRepository<MoneyAccount>>();
            accounts.Setup(a => a.GetAsync(It.IsAny<FilterFunc>())).ReturnsAsync(
                defaultFixture.CreateMany<MoneyAccount>(2).ToList()
            );
            var controller = new AccountsController(
                accounts.Object,
                new Mock<IAccountsService>().Object
            );

            var result = await controller.GetAccount("contains duplicates");

            var actual = result.Result as ObjectResult;
            Assert.Equal(500, actual.StatusCode);
        }

        [Fact]
        public async Task CreateTransaction()
        {
            var accounts = new Mock<IRepository<MoneyAccount>>();
            var service = new Mock<IAccountsService>();
            var controller = new AccountsController(accounts.Object, service.Object);
            var dto = defaultFixture.Create<TransactionDto.Create>();
            var account = defaultFixture.Create<MoneyAccount>();

            accounts.Setup(a => a.GetSingleAsync(account.Id)).ReturnsAsync(account);
            var result = await controller.CreateTransaction(account.Id, dto);

            Assert.IsType<OkObjectResult>(result.Result);
            Assert.NotNull((result.Result as OkObjectResult).Value);
            service.Verify(s => s.CreateAndApplyTransaction(It.IsAny<Transaction>(), account));
        }

        [Fact]
        public async Task CreateTransaction_BadAccountId()
        {
            var accounts = new Mock<IRepository<MoneyAccount>>();
            var service = new Mock<IAccountsService>();
            var controller = new AccountsController(accounts.Object, service.Object);
            var dto = defaultFixture.Create<TransactionDto.Create>();
            var account = defaultFixture.Create<MoneyAccount>();

            accounts.Setup(a => a.GetSingleAsync(It.IsAny<int>())).ReturnsAsync(() => null);
            var result = await controller.CreateTransaction(1, dto);

            Assert.IsType<BadRequestObjectResult>(result.Result);
        }
    }
}
