﻿namespace FinanceServerTests.TestUtilities
{
    public static class Fixtures
    {
        public static Fixture CreateNonRecursiveFixture()
        {
            var fixture = new Fixture();
            fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
                .ForEach(b => fixture.Behaviors.Remove(b));
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            return fixture;
        }

    }
}
