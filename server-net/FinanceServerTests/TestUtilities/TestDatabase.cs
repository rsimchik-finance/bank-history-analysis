﻿using FinanceServer.Data;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinanceServerTests.TestUtilities
{
    internal class TestDatabase : FinanceDatabase
    {
        public override string GetConnectionString()
        {
            return "server=127.0.0.1;port=3317;database=PersonalFinance;uid=financeapptester;pwd=testingmyefcoredatabase";
        }
    }
}
