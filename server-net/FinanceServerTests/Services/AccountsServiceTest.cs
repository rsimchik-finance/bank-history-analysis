﻿using FinanceServer.Models;
using FinanceServer.Repositories;
using FinanceServer.Services;

using FinanceServerTests.TestUtilities;

namespace FinanceServerTests.Services
{
    public class AccountsServiceTest
    {
        private static Fixture defaultFixture = Fixtures.CreateNonRecursiveFixture();

        [Fact]
        public async Task CreateAndApplyTransaction()
        {
            var mock = new AccountsServiceWithMockDependencies();
            var account = BuildAccountNoTransfers().Create();
            var transaction = defaultFixture.Build<Transaction>()
                .With(t => t.Account, () => null)
                .Create();

            var expectedBalance = account.Balance + transaction.Amount;
            await mock.Service.CreateAndApplyTransaction(transaction, account);

            Assert.Equal(account, transaction.Account);
            Assert.True(account.Transactions.Contains(transaction));
            Assert.Equal(expectedBalance, account.Balance);
            mock.Accounts.Verify(a => a.SaveAsync());
            mock.Transactions.Verify(t => t.SaveAsync());
        }

        [Fact]
        public async Task CreateAndApplyTransaction_AccountConflict()
        {
            var mock = new AccountsServiceWithMockDependencies();
            var account1 = BuildAccountNoTransfers().Create();
            var account2 = BuildAccountNoTransfers().Create();
            var transaction = defaultFixture.Build<Transaction>()
                .With(t => t.Account, account1)
                .Create();

            var invalidOperation = async () =>
            {
                await mock.Service.CreateAndApplyTransaction(transaction, account2);
            };

            await Assert.ThrowsAsync<InvalidOperationException>(invalidOperation);
        }

        [Fact]
        public async Task CreateAndApplyTransaction_AutoTransfer()
        {
            var mock = new AccountsServiceWithMockDependencies();
            var toAccount = BuildAccountNoTransfers().Create();
            var fromAccount = BuildAccountNoTransfers().Create();
            var transaction = defaultFixture.Build<Transaction>()
                .With(t => t.Account, fromAccount)
                .Create();
            var transfer = new AutoTransfer()
            {
                Source = fromAccount,
                Destination = toAccount,
                ShouldSubtractFromSource = false,
                Contribution = new decimal(new Random().NextDouble()),
                StartDate = transaction.Timestamp.AddDays(-1),
            };
            fromAccount.OutgoingAutoTransfers.Add(transfer);
            toAccount.IncomingAutoTransfers.Add(transfer);

            var toExpectedBalance = (int) (toAccount.Balance + (transaction.Amount * transfer.Contribution));
            var fromExpectedBalance = fromAccount.Balance + transaction.Amount;
            mock.Accounts.Setup(a => a.GetSingleAsync(toAccount.Id)).Returns(Task.FromResult(toAccount));
            await mock.Service.CreateAndApplyTransaction(transaction, fromAccount);

            Assert.Equal(fromExpectedBalance, fromAccount.Balance);
            Assert.Equal(toExpectedBalance, toAccount.Balance);
        }

        [Fact]
        public async Task CreateAndApplyAllTransactions()
        {
            var mock = new AccountsServiceWithMockDependencies();
            var account = BuildAccountNoTransfers().Create();
            var transactions = defaultFixture.Build<Transaction>()
                .With(t => t.Account, () => null)
                .With(t => t.Id, 0)
                .CreateMany();

            var expectedBalance = account.Balance + transactions.Sum(t => t.Amount);
            await mock.Service.CreateAndApplyAllTransactions(transactions, account);

            Assert.All(transactions, t => Assert.Equal(account, t.Account));
            Assert.Equal(account.Balance, expectedBalance);
            mock.Accounts.Verify(a => a.SaveAsync(), Times.Once());
            mock.Transactions.Verify(t => t.SaveAsync(), Times.Once());
        }

        [Fact]
        public async Task CreateAndApplyTransactions_AccountConflict()
        {
            var mock = new AccountsServiceWithMockDependencies();
            var account = BuildAccountNoTransfers().Create();
            var transactions = defaultFixture.Build<Transaction>()
                .With(t => t.Account, () => null)
                .With(t => t.Id, 0)
                .CreateMany();
            var objectToChange = transactions.Last();
            objectToChange.Account = BuildAccountNoTransfers().Create();

            mock.Transactions
                .Setup(t => t.HasChanges(It.IsAny<Transaction>()))
                .Returns<Transaction>(t => t != objectToChange);
            var invalidOperation = async () =>
            {
                await mock.Service.CreateAndApplyAllTransactions(transactions, account);
            };

            await Assert.ThrowsAsync<InvalidOperationException>(invalidOperation);
            mock.Transactions.Verify(
                t => t.UndoChanges(It.IsIn(transactions)),
                Times.Exactly(transactions.Count() - 1)
            );
            mock.Accounts.Verify(a => a.UndoChanges(account), Times.Once());
            mock.Accounts.Verify(a => a.SaveAsync(), Times.Never());
            mock.Transactions.Verify(t => t.SaveAsync(), Times.Never());
        }

        private AutoFixture.Dsl.IPostprocessComposer<MoneyAccount> BuildAccountNoTransfers()
        {
            return defaultFixture.Build<MoneyAccount>()
                .With(ma => ma.IncomingAutoTransfers, new List<AutoTransfer>())
                .With(ma => ma.OutgoingAutoTransfers, new List<AutoTransfer>());
        }
    }

    internal class AccountsServiceWithMockDependencies
    {
        public AccountsService Service { get; set; }
        public Mock<IRepository<MoneyAccount>> Accounts { get; set; }
        public Mock<IRepository<Transaction>> Transactions { get; set; }

        public AccountsServiceWithMockDependencies()
        {
            Accounts = new Mock<IRepository<MoneyAccount>>();
            Transactions = new Mock<IRepository<Transaction>>();
            Service = new AccountsService(Accounts.Object, Transactions.Object);
        }
    }
}
