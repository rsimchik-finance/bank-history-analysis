﻿using FinanceServer.Models;
using FinanceServer.Models.Dtos;

using FinanceServerTests.TestUtilities;

namespace FinanceServerTests.Models.Dtos
{
    public class TransactionDtoTest
    {
        private static Fixture defaultFixture = Fixtures.CreateNonRecursiveFixture();

        public class CreateTest
        {
            [Fact]
            public void ToModel()
            {
                var fixture = defaultFixture;
                var dto = fixture.Create<TransactionDto.Create>();

                var model = dto.ToModel();

                Assert.Equal(0, model.Id);
                Assert.Null(model.Account);
                Assert.Equal(dto.Amount, model.Amount);
                Assert.Equal(dto.Title, model.Title);
                Assert.Equal(dto.Description, model.Description);
                Assert.Equal(dto.Timestamp, model.Timestamp);
                Assert.Equal(dto.SourceId, model.SourceId);
            }
        }
    }
}
